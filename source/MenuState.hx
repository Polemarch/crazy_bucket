package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;

class MenuState extends FlxState
{
	private var text:FlxText;
	private var dx:Int = 0;
	private var dy:Int = 0;
	override public function create():Void
	{
		text = new FlxText(0, 0, 0, "Hello World!");
		super.create();
		add(text);
	}

	override public function update(elapsed:Float):Void {
		checkVel();
		moveText();
		
		super.update(elapsed);
	}

	private function checkVel():Void {
		if (FlxG.keys.pressed.UP){
			dy = -1;dx = 0;
		}else if (FlxG.keys.pressed.DOWN){
			dy = 1;dx=0;
		}else if (FlxG.keys.pressed.LEFT){
			dx = -1;dy=0;
		}else if (FlxG.keys.pressed.RIGHT){
			dx = 1;dy=0;
		}
	}
	private function moveText():Void {
		text.x+=dx;
		text.y+=dy;
	}
}
